var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0],
  main: [255, 255, 255],
  cell: [0, 0, 0]
}

var boardSize
var size = 51
var startSize = 5
var rule = 4
var stack = []
var growth = true
var array = create2DArray(size, size, 0, true)
stack.push(array)
var startArray = create2DArray(startSize, startSize, 0, false)
for (var i = 0; i < startArray.length; i++) {
  for (var j = 0; j < startArray[i].length; j++) {
    if (startArray[i][j] === 1) {
      array[i + Math.floor((array.length - startSize) * 0.5)][j + Math.floor((array.length - startSize) * 0.5)] = 1
    }
  }
}
stack.push(array)
var checkArray = create2DArray(size, size, false, true)
var frames = 0

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < array.length; i++) {
    for (var j = 0; j < array[i].length; j++) {
      noStroke()
      if (array[i][j] === 1) {
        fill(colors.main)
      } else if (array[i][j] === 0) {
        fill(colors.cell)
      }
      rect(windowWidth * 0.5 + (i - Math.floor(size * 0.5)) * (42 / 768) * boardSize * (17 / size), windowHeight * 0.5 + (j - Math.floor(size * 0.5)) * (42 / 768) * boardSize * (17 / size), (46 / 768) * boardSize * (17 / size), (46 / 768) * boardSize * (17 / size))
    }
  }

  frames += deltaTime * 0.025
  if (frames > 1) {
    frames = 0
    if (growth === true) {
      array = getNeighbours(array, rule)
      for (var i = 0; i < 10; i++) {
        array[Math.floor(Math.random() * array.length)][Math.floor(Math.random() * array.length)] = 1
      }
      stack.push(array)
    } else {
      if (stack.length > 0) {
        array = stack[stack.length - 1]
        stack.splice(-1, 1)
      }
    }

    if (stack.length === 0) {
      growth = true
      if (rule === 4) {
        rule = 5
      } else {
        rule = 4
      }
      array = create2DArray(size, size, 0, true)
      startArray = create2DArray(startSize, startSize, 0, false)
      for (var i = 0; i < startArray.length; i++) {
        for (var j = 0; j < startArray[i].length; j++) {
          if (startArray[i][j] === 1) {
            array[i + Math.floor((array.length - startSize) * 0.5)][j + Math.floor((array.length - startSize) * 0.5)] = 1
          }
        }
      }
    }

    if (array[0][0] + array[0][array.length - 1] + array[array.length - 1][0] + array[array.length - 1][array.length - 1] >= 2) {
      growth = false
    }
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = Math.floor(Math.random() * 2)
      }
    }
    array[i] = columns
  }
  return array
}

function getNeighbours(array, rule) {
  var neighbours = create2DArray(size, size, 0, true)
  for (var x = 0; x < array.length; x++) {
    for (var y = 0; y < array[x].length; y++) {
      if (x < array.length - 1) {
        if (array[x + 1][y] === 1) {
          neighbours[x][y]++
        }
      }
      if (x > 1) {
        if (array[x - 1][y] === 1) {
          neighbours[x][y]++
        }
      }
      if (y < array.length - 1) {
        if (array[x][y + 1] === 1) {
          neighbours[x][y]++
        }
      }
      if (y > 1) {
        if (array[x][y - 1] === 1) {
          neighbours[x][y]++
        }
      }
      if (x < array.length - 1 && y < array.length - 1) {
        if (array[x + 1][y + 1] === 1) {
          neighbours[x][y]++
        }
      }
      if (x < array.length - 1 && y > 1) {
        if (array[x + 1][y - 1] === 1) {
          neighbours[x][y]++
        }
      }
      if (x > 1 && y < array.length - 1) {
        if (array[x - 1][y + 1] === 1) {
          neighbours[x][y]++
        }
      }
      if (x > 1 && y > 1) {
        if (array[x - 1][y - 1] === 1) {
          neighbours[x][y]++
        }
      }
    }
  }
  var nextGeneration = create2DArray(size, size, 0, true)
  for (var i = 0; i < neighbours.length; i++) {
    for (var j = 0; j < neighbours[i].length; j++) {
      if (array[i][j] === 1) {
        if (neighbours[i][j] >= 1 && neighbours[i][j] <= rule) {
          nextGeneration[i][j] = 1
        } else if (neighbours[i][j] > rule || neighbours[i][j] === 0) {
          nextGeneration[i][j] = 0
        }
      }
      if (array[i][j] === 0 && neighbours[i][j] === 3) {
        nextGeneration[i][j] = 1
      }
    }
  }
  return nextGeneration
}
